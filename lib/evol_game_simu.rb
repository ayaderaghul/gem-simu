require "evol_game_simu/version"

# now i say again
# this time, it was also very tempting to do mutation
# but then please remember this bug
# you mutate at posn x, somehow it mutates also at posn y
# it breaks your simulation
# you cannot even manually set the value in posn y back
# wow

$Cooperator = [0, [[0, [0, 0]]]].freeze
$Defector = [0, [[1, [0, 0]]]].freeze
$Titfortat = [0, [[0, [0, 1]],
                  [1, [0, 1]]]].freeze
def Cooperator()
  $Cooperator.clone
end
def Defector()
  $Defector.clone
end
def Titfortat()
  $Titfortat.clone
end
$PAY = [[[3,3],[0,4]],[[4,0],[1,1]]].freeze
$ACTION = 2
$DELTA = 0.9
$ROUNDS = 400
def generate_deltas(delta, rounds)
  result = (0..rounds-1).to_a
  result.map { |round| delta ** round }
end
$DELTAS = generate_deltas($DELTA, $ROUNDS).freeze

def sum(arr)
  arr.reduce() { | total, n | total += n }
end
def interact(machine1,machine2,rounds)
  result = []
  current1, current2 = machine1[0],machine2[0]
  (1..rounds).reduce(result) do | result |    
    states1, states2 = machine1[1], machine2[1]
    cstate1, cstate2 = states1[current1], states2[current2]
    action1,action2 = cstate1[0], cstate2[0]
    dispatch1,dispatch2 = cstate1[1], cstate2[1]
    current1, current2 = dispatch1[action2], dispatch2[action1]
    result << $PAY[action1][action2]
  end
  result
end
def match_machines(machine1,machine2,rounds, deltas)
  pay1, pay2 = 0, 0
  current1, current2 = machine1[0], machine2[0]
  for i in 0..rounds-1 do
    states1, states2 = machine1[1], machine2[1]
    cstate1, cstate2 = states1[current1], states2[current2]
    action1,action2 = cstate1[0], cstate2[0]
    dispatch1,dispatch2 = cstate1[1], cstate2[1]
    current1,current2 = dispatch1[action2], dispatch2[action1]
    p1,p2 = $PAY[action1][action2]
    pay1 += p1*deltas[i]
    pay2 += p2*deltas[i]
  end
  return pay1.round(2), pay2.round(2)
end
def generate_dispatch(states)
  (0..$ACTION-1).map {|e| rand(states)}
end
def generate_state(states)
  [] << rand($ACTION) << generate_dispatch(states)
end
def generate_machine(states)
  init = rand(states)
  m = Array.new(states) { generate_state(states) }
  m = [init]+[m]
  m.freeze
end
def mutate_init(machine)
  init, ss = machine
  newinit = rand(ss.length)
  puts "initial state: #{machine[0]} -> #{newinit}"
  new = [newinit] + [ss]
  new.freeze
end

def mutate_action(machine)
  init, states = machine
  ss = states.length
  s = rand(ss)
  newact = rand($ACTION)
  puts "state #{s}: change action #{machine[1][s][0]} -> #{newact}"
  newss = []
  for i in 0..ss-1 do
    if i == s
      newss << [newact, states[i][1].clone]
    else
      newss << states[i].clone
    end
  end
  newss = [init] + [newss]
  newss.freeze
end

def mutate_dispatch(machine)
  init, states = machine
  ss = states.length
  s = rand(states.length)
  reactto = rand($ACTION)
  newdispatch=rand(ss)
  puts "state #{s}: change reaction to #{reactto} from #{machine[1][s][1][reactto]} -> #{newdispatch}"
  newss = []
  for i in 0..ss-1 do
    if i == s
      act, disp = states[i].clone
      toc, tod = disp
      if reactto == 0
        toc = newdispatch
      else
        tod = newdispatch
      end
      newss << [act, [toc, tod]]
    else
      newss << states[i].clone
    end
  end
  newss = [init] + [newss]
  newss.freeze
end
  
def add_state(machine)
  init, states = machine
  ss = states.length
  newss = ss + 1
  newstate = generate_state(newss)

  newstates = []
  s = rand(ss)
  moveendof = rand($ACTION)
  puts "add new state at the end: state #{s}: reaction to #{moveendof} goes to the new state"

  for i in 0..ss-1 do
    if i == s
      act, disp = states[i].clone
      toc, tod = disp
      if moveendof == 0
        toc = newss - 1
      else
        tod = newss - 1
      end
      newstates << [act, [toc, tod]]
    else
      newstates << states[i].clone
    end
  end
  newstates << newstate
  newstates = [init] + [newstates]
  newstates.freeze
end
def random_member(arr)
  arr[rand(arr.length)]
end
def del_state(machine)
  init, states = machine
  ss = states.length
  if ss == 1
    puts "\n"
    return machine.clone
  end
  pos = (0..ss-1).to_a
  s = rand(ss)
  pos.delete_at(s)
  newss = []
  for i in 0..ss-1 do
    act, dispatch = states[i]
    toc, tod = dispatch
    toc = random_member(pos) if toc == s
    tod = random_member(pos) if tod == s
    newss << [act, [toc, tod]]
  end
  puts "delete state #{s}"
  newss = [init] + [newss]
  newss.freeze
end
def mutate_machine(machine)
  r = rand(5)
  case r
  when 0
    mutate_init(machine)
  when 1
    mutate_action(machine)
  when 2
    mutate_dispatch(machine)
  when 3
    add_state(machine)
  when 4
    del_state(machine)
  end
end
def generate_population(n)
  Array.new(n) { generate_machine(1) }
end
def match_population(p,n,rounds,deltas)
  res = []
  for i in (0..n-2).step(2) do
    a,b = p[i],p[i+1]
    p1,p2 = match_machines(a,b,rounds,deltas)
    res << p1 << p2
  end
  res
end
def accumulate(arr)
  sofar = 0
  arr.reduce([]) do | res, n |
    sofar += n
    res << sofar
    res
  end
end
def throwonce(arr)
  m = arr.last
  r = rand(m)
  l = arr.length
  for i in 0..l-1 do
    e = arr[i]
    if r < e
      return i
    end
  end
  rand(l)
end

def throws(arr,s)
  m = arr.last
  l = arr.length
  res = []
  for i in 0..s-1 do
    r = rand(m)
    for j in 0..l-1 do
      e = arr[j]
      if r < e
        res << j
        break
      end
    end
    res << rand(l) if j == l-1
  end
  res
end

def regenerate(p,n,arr)
  subs = arr.map{|e| p[e]}
  l = subs.length
  part2 = p[l..n-1]
  newp = subs + part2
  newp.freeze
end
 
def mutate(p,n,m)
  muts = []
  for i in 0..m-1 do
    muts << mutate_machine(p[i])
  end
  part2 = p[m..n-1]
  newp = muts + part2
  newp.freeze
end

$N = 100
$P = generate_population $N
def evolve(n,cycles,speed,mutation,rounds,deltas)
  avg = []
  popu = generate_population $N
  for i in 1..cycles
    puts "before match"
    res = match_population popu,n,rounds,deltas
    avg << sum(res)/n

    a = accumulate(res)
    t = throws(a, speed)

    p2 = regenerate(popu,n,t)
    p3 = p2.shuffle
    p4 = mutate(p3,n,mutation)
    popu = p4
  end
  avg
end
